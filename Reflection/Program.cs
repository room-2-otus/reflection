﻿using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Running;
using Reflection;
using System.Diagnostics;
using System.Text.Json;
using System.Linq;

var csvSerializer = new CsvSerializer();
var csvSerializerExpTree = new CsvSerializerExpTree();
var csvDeserializer = new CsvDeserializer();
var csvDeserializerExpTree = new CsvDeserializerExpTree();
var stopwatch = new Stopwatch();

// Serialize and Deserialize with various object sizes
foreach (var size in Enumerable.Range(1, 10))
{
    var f = F.Get(size);

    // CSV Serialization
    stopwatch.Restart();
    for (int i = 0; i < 100000; i++)
    {
        csvSerializer.Serialize(f);
    }
    stopwatch.Stop();
    Console.WriteLine($"CSV serialization time for object size {size}: {stopwatch.ElapsedMilliseconds} ms");

    // CSV Serialization using expression trees
    stopwatch.Restart();
    for (int i = 0; i < 100000; i++)
    {
        csvSerializerExpTree.Serialize(f);
    }
    stopwatch.Stop();
    Console.WriteLine($"Using expression trees CSV serialization time for object size {size}: {stopwatch.ElapsedMilliseconds} ms");

    // JSON Serialization
    stopwatch.Restart();
    for (int i = 0; i < 100000; i++)
    {
        JsonSerializer.Serialize(f);
    }
    stopwatch.Stop();
    Console.WriteLine($"JSON serialization time for object size {size}: {stopwatch.ElapsedMilliseconds} ms");

    // CSV Deserialization
    var csvData = csvSerializer.Serialize(f);
    stopwatch.Restart();
    for (int i = 0; i < 100000; i++)
    {
        csvDeserializer.Deserialize<F>(csvData);
    }
    stopwatch.Stop();
    Console.WriteLine($"CSV deserialization time for object size {size}: {stopwatch.ElapsedMilliseconds} ms");

    // CSV Deserialization using expression trees
    var csvDataExpTree = csvSerializerExpTree.Serialize(f);
    stopwatch.Restart();
    for (int i = 0; i < 100000; i++)
    {
        csvDeserializerExpTree.Deserialize<F>(csvData);
    }
    stopwatch.Stop();
    Console.WriteLine($"Using expression trees CSV deserialization time for object size {size}: {stopwatch.ElapsedMilliseconds} ms");

    // JSON Deserialization
    var jsonData = JsonSerializer.Serialize(f);
    stopwatch.Restart();
    for (int i = 0; i < 100000; i++)
    {
        JsonSerializer.Deserialize<F>(jsonData);
    }
    stopwatch.Stop();
    Console.WriteLine($"JSON deserialization time for object size {size}: {stopwatch.ElapsedMilliseconds} ms");
}

// Run Benchmark
var config = DefaultConfig.Instance.AddDiagnoser(BenchmarkDotNet.Diagnosers.MemoryDiagnoser.Default);
var summary = BenchmarkRunner.Run<SerializationBenchmark>(config);
