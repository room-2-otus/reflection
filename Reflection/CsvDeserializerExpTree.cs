﻿using System;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Reflection
{
    public class CsvDeserializerExpTree
    {
        // Cache for property and field set delegates
        private static readonly Dictionary<Type, Dictionary<string, Action<object, object>>> _settersCache
            = new Dictionary<Type, Dictionary<string, Action<object, object>>>();

        public T Deserialize<T>(string csv) where T : new()
        {
            var lines = csv.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            var headers = lines[0].Split(',');
            var values = lines[1].Split(',');

            T obj = new T();

            // Ensure the setter delegates are prepared for this type
            EnsureSettersCache<T>();

            for (int i = 0; i < headers.Length; i++)
            {
                var setter = _settersCache[typeof(T)].GetValueOrDefault(headers[i]);
                if (setter != null)
                {
                    // Convert and set the value using the compiled delegate
                    setter(obj, Convert.ChangeType(values[i], GetMemberType(typeof(T).GetMember(headers[i])[0])));
                }
            }

            return obj;
        }

        private Type GetMemberType(MemberInfo member) => member.MemberType switch
        {
            MemberTypes.Field => ((FieldInfo)member).FieldType,
            MemberTypes.Property => ((PropertyInfo)member).PropertyType,
            _ => throw new InvalidOperationException("Unsupported member type")
        };

        private void EnsureSettersCache<T>() where T : new()
        {
            if (!_settersCache.ContainsKey(typeof(T)))
            {
                var typeSetters = new Dictionary<string, Action<object, object>>();
                var members = typeof(T).GetMembers(BindingFlags.Public | BindingFlags.Instance)
                                       .Where(m => m.MemberType == MemberTypes.Field || m.MemberType == MemberTypes.Property);

                foreach (var member in members)
                {
                    var paramObj = Expression.Parameter(typeof(object), "obj");
                    var paramVal = Expression.Parameter(typeof(object), "val");
                    var body = Expression.Assign(
                        Expression.MakeMemberAccess(Expression.Convert(paramObj, typeof(T)), member),
                        Expression.Convert(paramVal, GetMemberType(member))
                    );
                    var lambda = Expression.Lambda<Action<object, object>>(body, paramObj, paramVal);
                    typeSetters[member.Name] = lambda.Compile();
                }

                _settersCache[typeof(T)] = typeSetters;
            }
        }
    }
}
