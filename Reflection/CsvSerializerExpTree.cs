﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Collections.Concurrent;

namespace Reflection
{
    public class CsvSerializerExpTree
    {
        // Cache to store compiled expressions for faster access in subsequent calls
        private static readonly ConcurrentDictionary<Type, Delegate> _cachedFuncs = new ConcurrentDictionary<Type, Delegate>();

        public string Serialize<T>(T obj)
        {
            var type = typeof(T);
            var members = type.GetMembers(BindingFlags.Public | BindingFlags.Instance)
                              .Where(m => m.MemberType == MemberTypes.Field || m.MemberType == MemberTypes.Property);

            var csvBuilder = new StringBuilder();

            // Headers
            csvBuilder.AppendLine(string.Join(",", members.Select(m => m.Name)));

            // Using cached delegate if available, otherwise create and cache it
            var getValueFunc = (Func<object, string[]>)_cachedFuncs.GetOrAdd(type, t => CompileGetValueFunc(t, members));

            // Values
            csvBuilder.AppendLine(string.Join(",", getValueFunc(obj)));

            return csvBuilder.ToString();
        }

        private static Func<object, string[]> CompileGetValueFunc(Type type, IEnumerable<MemberInfo> members)
        {
            var param = Expression.Parameter(typeof(object), "obj");
            var castedParam = Expression.Convert(param, type);

            var memberAccessExpressions = members.Select(member =>
            {
                Expression memberAccess = member.MemberType switch
                {
                    MemberTypes.Field => Expression.Field(castedParam, (FieldInfo)member),
                    MemberTypes.Property => Expression.Property(castedParam, (PropertyInfo)member),
                    _ => throw new InvalidOperationException("Unsupported member type")
                };

                return Expression.Call(memberAccess, memberAccess.Type.GetMethod("ToString", new Type[0]));
            });

            var newArray = Expression.NewArrayInit(typeof(string), memberAccessExpressions);
            var lambda = Expression.Lambda<Func<object, string[]>>(newArray, param);

            return lambda.Compile();
        }
    }
}
