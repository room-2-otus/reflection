﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using System.Text.Json;
using System;
using System.Linq;
using Reflection;

public class SerializationBenchmark
{
    private F[] testObjects;
    private CsvSerializer csvSerializer;
    private CsvSerializerExpTree csvSerializerExpTree;
    private CsvDeserializer csvDeserializer;
    private CsvDeserializerExpTree csvDeserializerExpTree;

    public SerializationBenchmark()
    {
        // Prepare an array of test objects of varying sizes
        testObjects = Enumerable.Range(1, 10).Select(i => F.Get(i)).ToArray();
        csvSerializer = new CsvSerializer();
        csvSerializerExpTree = new CsvSerializerExpTree();
        csvDeserializer = new CsvDeserializer();
        csvDeserializerExpTree = new CsvDeserializerExpTree();
    }

    [Params(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)] // Corresponds to the index of testObjects
    public int ObjectIndex { get; set; }

    private F CurrentObject => testObjects[ObjectIndex];
    private string CurrentCsvData => csvSerializer.Serialize(CurrentObject);
    private string CurrentCsvDataExpTree => csvSerializerExpTree.Serialize(CurrentObject);

    [Benchmark]
    public void CsvSerialization()
    {
        csvSerializer.Serialize(CurrentObject);
    }

    [Benchmark]
    public void CsvSerializationExpTree()
    {
        csvSerializerExpTree.Serialize(CurrentObject);
    }

    [Benchmark]
    public void JsonSerialization()
    {
        JsonSerializer.Serialize(CurrentObject);
    }

    [Benchmark]
    public void CsvDeserialization()
    {
        csvDeserializer.Deserialize<F>(CurrentCsvData);
    }

    [Benchmark]
    public void CsvDeserializationExpTree()
    {
        csvDeserializerExpTree.Deserialize<F>(CurrentCsvDataExpTree);
    }

    [Benchmark]
    public void JsonDeserialization()
    {
        JsonSerializer.Deserialize<F>(JsonSerializer.Serialize(CurrentObject));
    }
}
