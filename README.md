# Reflection Serialization Benchmarking

Reflection project includes a custom implementation for CSV serialization and deserialization. 
This `README` provides details on the benchmarking setup, initially using a simple stopwatch for basic timing and then employing BenchmarkDotNet for a more comprehensive performance comparison. 
The benchmarking compares the performance of our custom CSV serializer/deserializer against standard JSON serializer/deserializer in .NET.

## Features

- Custom CSV serialization/deserialization.
- Standard JSON serialization/deserialization.
- Initial performance evaluation using a stopwatch.
- Advanced performance benchmarking using BenchmarkDotNet.

## Benchmark Setup

The benchmarking process involves two stages:

1. **Initial Timing with Stopwatch**: Basic performance measurement using the `System.Diagnostics.Stopwatch` class.
2. **Comprehensive Benchmarking using BenchmarkDotNet**: A detailed performance analysis comparing:
   - Custom CSV Serialization/Deserialization.
   - .NET's built-in JSON Serialization/Deserialization.

## Installation

To run the benchmarks on your local machine, ensure you have the following installed:
- .NET 7 SDK or later.
- BenchmarkDotNet package (can be installed via NuGet).

## Running the Benchmarks

### Initial Stopwatch Benchmark

Navigate to the project directory in your terminal and execute the stopwatch-based tests to get a preliminary understanding of the performance.

### Advanced BenchmarkDotNet Analysis

For a more detailed performance analysis, run:

dotnet run -c Release


## Benchmark Class

The `SerializationBenchmark` class in the project contains methods for benchmarking the following operations:

- `CsvSerialization()`: Serializes an object to CSV using the custom serializer.
- `JsonSerialization()`: Serializes an object to JSON using .NET's JsonSerializer.
- `CsvDeserialization()`: Deserializes a CSV string to an object using the custom deserializer.
- `JsonDeserialization()`: Deserializes a JSON string to an object using .NET's JsonSerializer.

## Results

You will receive two sets of results:

1. **Stopwatch Results**: Quick and basic timing results for an initial performance overview.
2. **BenchmarkDotNet Results**: Detailed report in the console output, including metrics such as mean time, error margin, and memory allocation.

## Performance Benchmark Analysis

I have conducted performance benchmarks using both a manual approach with a stopwatch and a more detailed approach using the BenchmarkDotNet framework. Below is a summary and analysis of the results from both methods.

### Manual Benchmark Using Stopwatch

- **CSV Serialization**: 102 ms
- **JSON Serialization**: 23 ms
- **CSV Deserialization**: 128 ms
- **JSON Deserialization**: 12 ms

### BenchmarkDotNet Results

| Method              | Mean      | Error     | StdDev    | Gen0   | Allocated |
|-------------------- |----------:|----------:|----------:|-------:|----------:|
| CsvSerialization    | 610.86 ns | 10.193 ns |  9.534 ns | 0.0772 |    1016 B |
| JsonSerialization   |  66.10 ns |  0.612 ns |  0.572 ns | 0.0024 |      32 B |
| CsvDeserialization  | 939.86 ns | 11.900 ns | 10.549 ns | 0.1049 |    1392 B |
| JsonDeserialization | 158.34 ns |  0.839 ns |  0.785 ns | 0.0055 |      72 B |

### Analysis

- The manual stopwatch measurements, given in milliseconds, show a noticeable difference in performance between CSV and JSON operations, with JSON being significantly faster in both serialization and deserialization.
- The BenchmarkDotNet results, presented in nanoseconds, provide a more granular view of the performance. They also reflect the faster performance of JSON operations compared to CSV.
- In both tests, JSON serialization and deserialization are substantially faster than CSV. This can be attributed to the efficiency of .NET's built-in JSON serializers/deserializers and the relatively heavier processing required for CSV format, which involves string manipulations and reflection.
- The difference in performance metrics between the manual stopwatch and BenchmarkDotNet can be attributed to the different levels of precision and the methodology of measurement. BenchmarkDotNet provides a more detailed analysis, including error margins and standard deviations.

### Considerations
Reflection Overhead: The custom CSV serializer/deserializer uses reflection, which is generally slower compared to direct field/property access. This could be a significant contributor to the performance difference.
String Operations: CSV handling involves more string concatenation and parsing, which are relatively expensive operations compared to the binary and optimized processing in JSON serialization/deserialization.
Optimization Possibilities: There might be room for optimization in the custom CSV serializer/deserializer, such as caching reflection results or optimizing string handling.
Use Case Suitability: Depending on the specific requirements of the application, the trade-off in performance might be acceptable if CSV format is a requirement. For example, if human readability or interoperability with systems that require CSV is important, the overhead might be justified.

### Conclusion

Based on the benchmark results, it is evident that for scenarios where performance is a critical factor, JSON serialization/deserialization is preferable over CSV. However, the choice of format should also consider other factors like human readability, file format requirements, and ease of data interchange with other systems.
While the custom CSV serializer/deserializer works as intended, it's significantly less performant compared to the built-in JSON mechanisms in .NET. This performance difference should be taken into account when deciding which serialization/deserialization method to use in an application. If performance is a critical factor and the CSV format is not a strict requirement, using JSON might be the more efficient choice.


# Performance Comparison: Traditional vs Expression Trees Based Serialization/Deserialization

The following results demonstrate the performance differences between the traditional reflection-based approach and the optimized approach using expression trees for serialization and deserialization.

## Stopwatch Test Results

The results below show the time taken for CSV and JSON serialization and deserialization operations for objects of different sizes, comparing traditional methods and methods using expression trees:

CSV serialization time for object size 1: 142 ms
Using expression trees CSV serialization time for object size 1: 94 ms
JSON serialization time for object size 1: 22 ms
CSV deserialization time for object size 1: 270 ms
Using expression trees CSV deserialization time for object size 1: 272 ms
JSON deserialization time for object size 1: 12 ms
CSV serialization time for object size 2: 131 ms
Using expression trees CSV serialization time for object size 2: 77 ms
JSON serialization time for object size 2: 13 ms
CSV deserialization time for object size 2: 195 ms
Using expression trees CSV deserialization time for object size 2: 193 ms
JSON deserialization time for object size 2: 12 ms
CSV serialization time for object size 3: 103 ms
Using expression trees CSV serialization time for object size 3: 59 ms
JSON serialization time for object size 3: 6 ms
CSV deserialization time for object size 3: 191 ms
Using expression trees CSV deserialization time for object size 3: 189 ms
JSON deserialization time for object size 3: 8 ms
CSV serialization time for object size 4: 111 ms
Using expression trees CSV serialization time for object size 4: 66 ms
JSON serialization time for object size 4: 7 ms
CSV deserialization time for object size 4: 205 ms
Using expression trees CSV deserialization time for object size 4: 194 ms
JSON deserialization time for object size 4: 9 ms
CSV serialization time for object size 5: 105 ms
Using expression trees CSV serialization time for object size 5: 59 ms
JSON serialization time for object size 5: 7 ms
CSV deserialization time for object size 5: 195 ms
Using expression trees CSV deserialization time for object size 5: 190 ms
JSON deserialization time for object size 5: 8 ms
CSV serialization time for object size 6: 105 ms
Using expression trees CSV serialization time for object size 6: 60 ms
JSON serialization time for object size 6: 7 ms
CSV deserialization time for object size 6: 199 ms
Using expression trees CSV deserialization time for object size 6: 190 ms
JSON deserialization time for object size 6: 8 ms
CSV serialization time for object size 7: 105 ms
Using expression trees CSV serialization time for object size 7: 59 ms
JSON serialization time for object size 7: 6 ms
CSV deserialization time for object size 7: 196 ms
Using expression trees CSV deserialization time for object size 7: 197 ms
JSON deserialization time for object size 7: 9 ms
CSV serialization time for object size 8: 108 ms
Using expression trees CSV serialization time for object size 8: 61 ms
JSON serialization time for object size 8: 11 ms
CSV deserialization time for object size 8: 196 ms
Using expression trees CSV deserialization time for object size 8: 193 ms
JSON deserialization time for object size 8: 10 ms
CSV serialization time for object size 9: 105 ms
Using expression trees CSV serialization time for object size 9: 62 ms
JSON serialization time for object size 9: 9 ms
CSV deserialization time for object size 9: 201 ms
Using expression trees CSV deserialization time for object size 9: 193 ms
JSON deserialization time for object size 9: 9 ms
CSV serialization time for object size 10: 109 ms
Using expression trees CSV serialization time for object size 10: 60 ms
JSON serialization time for object size 10: 7 ms
CSV deserialization time for object size 10: 197 ms
Using expression trees CSV deserialization time for object size 10: 193 ms
JSON deserialization time for object size 10: 9 ms

## Benchmark Test Results

The table below shows the results of the benchmark tests, including mean execution time, error, standard deviation, and memory allocation for different object sizes:

| Method                    | ObjectIndex | Mean        | Error     | StdDev    | Gen0   | Allocated |
|-------------------------- |------------ |------------:|----------:|----------:|-------:|----------:|
| CsvSerialization          | 0           |   981.51 ns |  7.148 ns |  6.687 ns | 0.1144 |    1504 B |
| CsvSerializationExpTree   | 0           |   543.07 ns |  1.930 ns |  1.507 ns | 0.1001 |    1312 B |
| JsonSerialization         | 0           |    65.63 ns |  0.325 ns |  0.304 ns | 0.0024 |      32 B |
| CsvDeserialization        | 0           | 3,140.71 ns | 55.535 ns | 70.234 ns | 0.2899 |    3816 B |
| CsvDeserializationExpTree | 0           | 2,591.12 ns | 31.395 ns | 27.830 ns | 0.2251 |    2968 B |
| JsonDeserialization       | 0           |   161.13 ns |  0.728 ns |  0.569 ns | 0.0067 |      88 B |
| CsvSerialization          | 1           | 1,028.58 ns |  7.271 ns |  6.445 ns | 0.1316 |    1720 B |
| CsvSerializationExpTree   | 1           |   586.58 ns |  1.617 ns |  1.262 ns | 0.1163 |    1528 B |
| JsonSerialization         | 1           |    66.42 ns |  0.290 ns |  0.242 ns | 0.0024 |      32 B |
| CsvDeserialization        | 1           | 3,212.14 ns | 49.058 ns | 45.889 ns | 0.3128 |    4088 B |
| CsvDeserializationExpTree | 1           | 2,584.32 ns | 20.102 ns | 17.820 ns | 0.2441 |    3240 B |
| JsonDeserialization       | 1           |   161.42 ns |  0.887 ns |  0.786 ns | 0.0067 |      88 B |
| CsvSerialization          | 2           | 1,029.70 ns |  4.631 ns |  3.867 ns | 0.1335 |    1760 B |
| CsvSerializationExpTree   | 2           |   595.44 ns | 11.185 ns | 10.462 ns | 0.1192 |    1568 B |
| JsonSerialization         | 2           |    66.85 ns |  0.426 ns |  0.378 ns | 0.0024 |      32 B |
| CsvDeserialization        | 2           | 3,197.43 ns | 43.088 ns | 40.305 ns | 0.3166 |    4144 B |
| CsvDeserializationExpTree | 2           | 2,643.74 ns | 20.393 ns | 18.078 ns | 0.2518 |    3296 B |
| JsonDeserialization       | 2           |   164.51 ns |  0.751 ns |  0.665 ns | 0.0067 |      88 B |
| CsvSerialization          | 3           | 1,055.91 ns |  8.664 ns |  7.235 ns | 0.1354 |    1792 B |
| CsvSerializationExpTree   | 3           |   601.56 ns |  4.331 ns |  3.617 ns | 0.1221 |    1600 B |
| JsonSerialization         | 3           |    66.18 ns |  0.476 ns |  0.445 ns | 0.0024 |      32 B |
| CsvDeserialization        | 3           | 3,254.30 ns | 52.611 ns | 46.638 ns | 0.3166 |    4184 B |
| CsvDeserializationExpTree | 3           | 2,612.31 ns | 26.163 ns | 20.426 ns | 0.2518 |    3336 B |
| JsonDeserialization       | 3           |   162.84 ns |  0.892 ns |  0.835 ns | 0.0067 |      88 B |
| CsvSerialization          | 4           | 1,069.19 ns | 21.224 ns | 39.865 ns | 0.1392 |    1832 B |
| CsvSerializationExpTree   | 4           |   622.68 ns | 12.457 ns | 11.652 ns | 0.1249 |    1640 B |
| JsonSerialization         | 4           |    66.80 ns |  0.875 ns |  0.819 ns | 0.0024 |      32 B |
| CsvDeserialization        | 4           | 3,104.70 ns | 29.845 ns | 24.922 ns | 0.3204 |    4232 B |
| CsvDeserializationExpTree | 4           | 2,535.96 ns | 17.183 ns | 16.073 ns | 0.2556 |    3384 B |
| JsonDeserialization       | 4           |   157.66 ns |  0.608 ns |  0.539 ns | 0.0067 |      88 B |
| CsvSerialization          | 5           | 1,040.81 ns | 20.013 ns | 20.552 ns | 0.1392 |    1832 B |
| CsvSerializationExpTree   | 5           |   589.93 ns |  7.941 ns |  7.040 ns | 0.1249 |    1640 B |
| JsonSerialization         | 5           |    66.86 ns |  0.543 ns |  0.453 ns | 0.0024 |      32 B |
| CsvDeserialization        | 5           | 3,330.66 ns | 66.346 ns | 78.981 ns | 0.3204 |    4232 B |
| CsvDeserializationExpTree | 5           | 2,686.32 ns | 48.697 ns | 43.169 ns | 0.2556 |    3384 B |
| JsonDeserialization       | 5           |   159.90 ns |  1.829 ns |  1.711 ns | 0.0067 |      88 B |
| CsvSerialization          | 6           | 1,054.89 ns | 17.707 ns | 15.697 ns | 0.1392 |    1832 B |
| CsvSerializationExpTree   | 6           |   597.68 ns | 11.885 ns | 11.118 ns | 0.1249 |    1640 B |
| JsonSerialization         | 6           |    65.95 ns |  0.633 ns |  0.592 ns | 0.0024 |      32 B |
| CsvDeserialization        | 6           | 3,185.79 ns | 61.998 ns | 76.139 ns | 0.3204 |    4232 B |
| CsvDeserializationExpTree | 6           | 2,584.98 ns | 37.716 ns | 35.279 ns | 0.2556 |    3384 B |
| JsonDeserialization       | 6           |   159.26 ns |  1.645 ns |  1.539 ns | 0.0067 |      88 B |
| CsvSerialization          | 7           | 1,074.54 ns | 21.150 ns | 24.357 ns | 0.1392 |    1832 B |
| CsvSerializationExpTree   | 7           |   622.50 ns | 12.336 ns | 16.468 ns | 0.1249 |    1640 B |
| JsonSerialization         | 7           |    65.91 ns |  0.647 ns |  0.605 ns | 0.0024 |      32 B |
| CsvDeserialization        | 7           | 3,198.85 ns | 20.619 ns | 19.287 ns | 0.3204 |    4232 B |
| CsvDeserializationExpTree | 7           | 2,626.68 ns | 15.954 ns | 14.924 ns | 0.2556 |    3384 B |
| JsonDeserialization       | 7           |   162.25 ns |  0.674 ns |  0.631 ns | 0.0067 |      88 B |
| CsvSerialization          | 8           | 1,053.00 ns | 14.211 ns | 13.293 ns | 0.1392 |    1832 B |
| CsvSerializationExpTree   | 8           |   593.65 ns |  7.468 ns |  6.986 ns | 0.1249 |    1640 B |
| JsonSerialization         | 8           |    66.55 ns |  0.178 ns |  0.157 ns | 0.0024 |      32 B |
| CsvDeserialization        | 8           | 3,137.94 ns | 10.576 ns |  9.375 ns | 0.3204 |    4232 B |
| CsvDeserializationExpTree | 8           | 2,596.40 ns | 35.574 ns | 31.536 ns | 0.2556 |    3384 B |
| JsonDeserialization       | 8           |   166.56 ns |  1.916 ns |  1.792 ns | 0.0067 |      88 B |
| CsvSerialization          | 9           | 1,081.35 ns | 17.660 ns | 16.519 ns | 0.1431 |    1872 B |
| CsvSerializationExpTree   | 9           |   599.96 ns |  6.531 ns |  5.454 ns | 0.1278 |    1680 B |
| JsonSerialization         | 9           |    66.44 ns |  0.762 ns |  0.713 ns | 0.0024 |      32 B |
| CsvDeserialization        | 9           | 3,149.83 ns | 44.978 ns | 42.073 ns | 0.3281 |    4288 B |
| CsvDeserializationExpTree | 9           | 2,527.24 ns | 10.971 ns |  9.161 ns | 0.2632 |    3440 B |
| JsonDeserialization       | 9           |   161.35 ns |  2.508 ns |  2.223 ns | 0.0067 |      88 B |


## Analysis

- The use of expression trees in CSV serialization significantly improves performance compared to the traditional reflection-based approach.
- For deserialization, the performance gains are less pronounced, but the expression tree approach still shows a marginal improvement.
- As expected, JSON serialization and deserialization are consistently faster than CSV, irrespective of the approach used for CSV.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

